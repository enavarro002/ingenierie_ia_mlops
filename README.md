## Project team
- Emma Navarro
- Paul Lorgue
- Albane Arthuis

## Deployed on Heroku
https://ingenierie-ia-mlops.herokuapp.com/

## Subject
https://tinyurl.com/mlops-paper 

## Code coverage
Using pytest-cov code coverage, we get 100% code coverage even if two functions of main aren't tested.

## Documentation automatique
https://ingenierie-ia-mlops.herokuapp.com/docs

## Benchmark
1. "Vos services sont vachement gourmand coté ML. Tu peux me fournir la taille de ton image docker et la limite de RAM à définir sur les environnements stp ?"
La taille de notre image Docker est de 375,07MB. 

2. "Le modèle est viable jusqu’à quel trafic en production ? On souhaite avoir un P99 < 200ms"
![](images/175.png)
Pour 175 utilisateurs 95% des réponses ont un response time inférieur à 170ms.
![](images/177.png)
Pour 177 utilisateurs on passe à 240ms maximum. En supposant que cette augmenttion soit linéaire 176 utilisateurs ne seraient pas conformes à P99 < 200ms.
Il est donc préférable d'avoir un maximum de 175 utilisateurs sur le site simultanément.

3. "On veut enrichir de la donnée dans la stack data avec ton modèle mais ça rame fort, t'as des idées pour améliorer les perfs ?"
Pour améliorer les performances du modèle, nous pourrions regrouper des requêtes utilisateurs par batch et y répondre simultanément. On peut également penser à faire de l'incremental learning pour s'adapter aux demandes qui nous sont faites et nouvelles façon de parler.