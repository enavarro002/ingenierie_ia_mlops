from locust import HttpUser, task, between

class User(HttpUser):
    wait_time = between(1, 5)

    @task
    def health(self):
        self.client.get("/health")

    @task(2)
    def api_intent_supported_language(self):
        self.client.get("/api/intent-supported-languages")

    @task(5)
    def api_intent_sentence(self):
        sentences_examples=["J'adore les fraises", "Où est la boulangerie la plus proche", "J'ai soif", "Mon ami est blessé", "aaaah"]
        for sentence in sentences_examples :
            request="/api/intent/"+sentence
            self.client.get(request)
        #self.client.get("/api/intent/coucou")
        
        