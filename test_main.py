from json import dumps
import pytest
import spacy
import main

nlp = spacy.load("./models")

def test_root_endpoint():
    assert main.root_endpoint()==dumps(["Hello world !"])


def test_intent_inference():
    resp = main.intent_inference("bonjour test")
    assert type(resp) is str


def test_supported_languages():
    assert main.supported_languages()== dumps(["fr-FR"])


def test_health_check_endpoint():
    assert main.health_check_endpoint().status_code==200