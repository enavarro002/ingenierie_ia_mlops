import bottle
import spacy
import os, logging
import uvicorn
from json import dumps
from fastapi import FastAPI, responses

app = FastAPI()

logging.info("Loading model..")
nlp = spacy.load("./models")


@app.get("/")
#@bottle.route('/')
def root_endpoint():
    return dumps(["Hello world !"])

@app.get("/api/intent/{sentence}")
#@bottle.route("/api/intent/<sentence>")
def intent_inference(sentence) :
    #sentence = bottle.request.query['sentence']
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != "textcat"]
    with nlp.disable_pipes(*other_pipes):
        inference = nlp(sentence)
    #bottle.response.content_type = "application/json"
    return dumps(inference.cats)

@app.get("/api/intent-supported-languages")
#@bottle.route("/api/intent-supported-languages")
def supported_languages():
    #bottle.response.content_type = "application/json"
    #print(dumps(["fr-FR"]))
    return dumps(["fr-FR"])

@app.get("/health")
#@bottle.route("/health")
def health_check_endpoint():
    #return bottle.HTTPResponse(status=200)
    return responses.JSONResponse(status_code=200)

# if __name__ == "__main__":
#     bottle.run(bottle.app(), host='0.0.0.0', port=int(os.environ.get('PORT', 8080)), debug=True, reloader=True)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=int(os.environ.get('PORT', 8080)))
