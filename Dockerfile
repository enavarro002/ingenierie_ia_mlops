# syntax=docker/dockerfile:1

FROM python:3.8-slim-buster

COPY . .

RUN pip install -r requirements.txt
RUN pip install pytest
RUN pip install fastapi
RUN pip install uvicorn

RUN pip install poetry==1.0
RUN poetry config virtualenvs.create false
RUN poetry install --no-dev

EXPOSE 8080

CMD ["python", "-u", "main.py"]